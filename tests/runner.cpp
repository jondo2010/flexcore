#define BOOST_TEST_MODULE core_module
#include <boost/test/included/unit_test.hpp>

#include <fstream>

struct Config
{
    Config()
    {
        const char* xml_out = std::getenv("XML_OUTPUT_FILE");
        if (xml_out)
        {
            test_log = std::ofstream(xml_out);
            boost::unit_test::unit_test_log.set_stream(test_log);
            boost::unit_test::unit_test_log.set_format(boost::unit_test::OF_JUNIT);
        }
    }
    ~Config() { boost::unit_test::unit_test_log.set_stream(std::cout); }
    std::ofstream test_log;
};

BOOST_TEST_GLOBAL_CONFIGURATION(Config);
