cc_library(
    name = "adobe",
    srcs = [],
    hdrs = [
        "flexcore/3rdparty/adobe/config.hpp",
        "flexcore/3rdparty/adobe/forest.hpp",
        "flexcore/3rdparty/adobe/reverse.hpp",
        "flexcore/3rdparty/adobe/set_next.hpp",
    ],
    includes = ["flexcore/3rdparty"],
)

cc_library(
    name = "flexcore",
    srcs = [
        "flexcore/infrastructure.cpp",
        "flexcore/extended/graph/graph.cpp",
        "flexcore/utils/logging/logger.cpp",
        "flexcore/utils/demangle.cpp",
        "flexcore/extended/base_node.cpp",
        "flexcore/extended/visualization/visualization.cpp",
        "flexcore/scheduler/clock.cpp",
        "flexcore/scheduler/cyclecontrol.cpp",
        "flexcore/scheduler/parallelregion.cpp",
        "flexcore/scheduler/parallelscheduler.cpp",
        "flexcore/scheduler/serialschedulers.cpp",
    ],
    hdrs = [
        "flexcore/ports.hpp",
        "flexcore/infrastructure.hpp",
    ] + glob([
        "flexcore/utils/**/*.hpp",
        "flexcore/scheduler/**/*.hpp",
        "flexcore/extended/**/*.hpp",
        "flexcore/core/**/*.hpp",
        "flexcore/pure/**/*.hpp",
        "flexcore/range/*.hpp",
    ]),
    include_prefix = "flexcore",
    includes = ["flexcore"],
    strip_include_prefix = "flexcore",
    deps =[
        ":adobe",
        "@boost//:core",
        "@boost//:uuid",
        "@boost//:graph",
        "@boost//:circular_buffer",
        "@boost//:log",
    ],
    copts = [
        "-std=c++14",
    ],
    linkopts = [
        #"-Wl,--no-undefined"
    ],
    #linkstatic = False,
    #alwayslink = True,
    visibility = ["//visibility:public"]
)

cc_binary(
    name = "dummy_executable",
    srcs = ["integration_tests/main.cpp"],
    includes = ["flexcore"],
    copts = [
        "-std=c++14",
    ],
    deps = [
        "//:flexcore",
        "@boost//:scope_exit",
    ]
)

cc_test(
    name = "test_executable",
    srcs = [
        "tests/examples.cpp",

        "tests/core/test_connection.cpp",
        "tests/core/test_connectables.cpp",
        "tests/core/test_traits.cpp",

        "tests/logging/test_logging.cpp",

        "tests/nodes/test_buffer.cpp",
        "tests/nodes/test_generic.cpp",
        "tests/nodes/test_event_nodes.cpp",
        "tests/nodes/test_state_nodes.cpp",
        "tests/nodes/test_moving.cpp",

        "tests/extended/graph/test_graph.cpp",
        "tests/extended/nodes/test_base_node.cpp",
        "tests/extended/nodes/test_infrastructure.cpp",
        "tests/extended/nodes/test_region_worker_node.cpp",
        "tests/extended/nodes/test_terminal_node.cpp",
        "tests/extended/ports/test_node_aware.cpp",
        "tests/extended/ports/test_region_buffer.cpp",

        "tests/pure/test_events.cpp",
        "tests/pure/test_moving.cpp",
        "tests/pure/test_mux_ports.cpp",
        "tests/pure/test_state_sinks.cpp",

        "tests/range/test_range.cpp",

        #tests/"serialisation/test_deserializer.cpp",

        #tests/"settings/test_settings.cpp",
        #tests/"settings/test_setting_backend.cpp",

        "tests/scheduler/TestClock.cpp",
        "tests/scheduler/test_cyclecontrol.cpp",
        "tests/scheduler/test_parallel_region.cpp",
        "tests/scheduler/test_parallelscheduler.cpp",
        "tests/scheduler/test_serialscheduler.cpp",

        #tests/"util/test_generic_container.cpp",

        "tests/runner.cpp",

        "tests/pure/sink_fixture.hpp",
        "tests/core/movable_connectable.hpp",
        "tests/nodes/owning_node.hpp",
    ],
    deps = [
        ":flexcore",
        "@boost//:test",
    ],
    copts = [
        "-std=c++14",
    ],
    linkstatic = False,
    size = "large",
    args = [
        "--log_level=all",
    ],
)
